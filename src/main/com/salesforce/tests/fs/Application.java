package com.salesforce.tests.fs;

import com.salesforce.tests.fs.service.FileSystem;
import com.salesforce.tests.fs.service.commands.CommandConstants;
import com.salesforce.tests.fs.service.executor.CommandExecutor;

import java.util.*;


public class Application {

    public static void main(String[] args) {

        FileSystem fileSystem = new FileSystem();
        CommandExecutor commandExecutor = new CommandExecutor();

        Scanner in = new Scanner(System.in);
        String command = in.nextLine();

        String[] arguments = command.split("\\s+");

        while (!arguments[0].equals(CommandConstants.QUIT)){
            System.out.println("--- Command "+command+" ------");

            commandExecutor.executeCommand(arguments, fileSystem);

            command = in.nextLine();
            arguments = command.split("\\s+");
        }
    }

}
