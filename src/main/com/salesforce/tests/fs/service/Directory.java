package com.salesforce.tests.fs.service;
import java.util.HashMap;

public class Directory {

    private String name;
    private boolean isRoot;
    private boolean isFile;
    private String path;
    private Directory parent;

    private HashMap<String, Directory> cache;

    public Directory(String name, Directory parent, boolean isFile) {
        this.name = name;
        this.isRoot = false;
        this.isFile = isFile;
        this.parent = parent;

        if (parent != null) {
            this.path = isFile ? parent.getPath() + name : parent.getPath() + name + "/";
        } else {
            this.path = "";
        }

        this.cache = new HashMap<>();
    }

    public boolean isRoot() {
        return isRoot;
    }

    public void setRoot(boolean root) {
        isRoot = root;
    }


    public HashMap<String, Directory> getCache() {
        return cache;
    }

    public void setCache(HashMap<String, Directory> cache) {
        this.cache = cache;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isFile() {
        return isFile;
    }

    public void setFile(boolean file) {
        isFile = file;
    }

    public Directory getParent() {
        return parent;
    }

    public void setParent(Directory parent) {
        this.parent = parent;
    }
}
