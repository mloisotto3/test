package com.salesforce.tests.fs.service;

import java.util.HashMap;

public class FileSystem {

    private HashMap<String, Directory> fileSystemMap;
    private Directory currentDirectory;

    public FileSystem() {
        this.currentDirectory = new Directory("", null, false);
        this.fileSystemMap = new HashMap<>();
    }

    public void createDirectory(Directory directory) {
        HashMap<String, Directory> cacheDirectory = currentDirectory.getCache();

        if (cacheDirectory.containsKey(directory.getName())) {
            System.out.println("Directory already exists");
        } else {
            cacheDirectory.put(directory.getName(), directory);
        }
    }

    public Directory getCurrentDirectory() {
        return currentDirectory;
    }

    public void createFile(Directory directory) {

        HashMap<String, Directory> cacheDirectory = currentDirectory.getCache();

        if (cacheDirectory.containsKey(directory.getName())) {
            System.out.println("File already exists");
        } else {
            cacheDirectory.put(directory.getName(), directory);
        }
    }

    public void changeDirectory(String directory) {
        HashMap<String, Directory> cacheDirectory = currentDirectory.getCache();

        if (directory.equals("..")) {
            currentDirectory = currentDirectory.getParent();
        } else {
            if (cacheDirectory.containsKey(directory)) {
                Directory newDirectory = cacheDirectory.get(directory);
                if (!newDirectory.isFile()) {
                    currentDirectory = newDirectory;
                } else {
                    System.out.println("Cannot access, it is a file");
                }
            } else {
                System.out.println("Directory not found");
            }
        }
    }

    public void printCurrentDirectory() {
        System.out.println(currentDirectory.getPath());
    }




}
