package com.salesforce.tests.fs.service.commands;

import com.salesforce.tests.fs.service.Directory;
import com.salesforce.tests.fs.service.FileSystem;

public class MkdirCommand implements Command {


    private FileSystem fileSystem;
    private Directory directory;


    public MkdirCommand(String directory, FileSystem fileSystem) {
        this.fileSystem = fileSystem;
        this.directory = new Directory(directory, fileSystem.getCurrentDirectory(), false);
    }


    @Override
    public void execute() {
        fileSystem.createDirectory(directory);
    }
}
