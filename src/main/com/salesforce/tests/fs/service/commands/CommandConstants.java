package com.salesforce.tests.fs.service.commands;

public class CommandConstants {

    public static final String QUIT = "quit";
    public static final String PWD = "pwd";
    public static final String LS = "ls";
    public static final String MKDIR = "mkdir";
    public static final String TOUCH = "touch";
    public static final String CD = "cd";

}
