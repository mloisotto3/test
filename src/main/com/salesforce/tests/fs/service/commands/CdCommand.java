package com.salesforce.tests.fs.service.commands;

import com.salesforce.tests.fs.service.FileSystem;

public class CdCommand implements Command {

    private FileSystem fileSystem;
    private String directory;


    public CdCommand(String directory, FileSystem fileSystem) {
        this.fileSystem = fileSystem;
        this.directory = directory;
    }

    @Override
    public void execute() {
        fileSystem.changeDirectory(directory);
    }
}
