package com.salesforce.tests.fs.service.commands;

import com.salesforce.tests.fs.service.FileSystem;


public class PwdCommand implements Command {

    private FileSystem fileSystem;


    public PwdCommand(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    @Override
    public void execute() {
        fileSystem.printCurrentDirectory();
    }
}
