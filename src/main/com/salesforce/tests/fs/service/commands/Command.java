package com.salesforce.tests.fs.service.commands;
public interface Command {

    void execute();
}
