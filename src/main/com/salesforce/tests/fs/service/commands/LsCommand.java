package com.salesforce.tests.fs.service.commands;

import com.salesforce.tests.fs.service.Directory;
import com.salesforce.tests.fs.service.FileSystem;

import java.util.HashMap;
import java.util.Set;

public class LsCommand implements Command {

    private FileSystem fileSystem;


    public LsCommand(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }



    @Override
    public void execute() {
        Directory directory = fileSystem.getCurrentDirectory();

        HashMap<String, Directory> cache = directory.getCache();

        Set<String> keys = cache.keySet();
        for (String key : keys) {
            System.out.println(key);
        }
    }
}
