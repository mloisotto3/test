package com.salesforce.tests.fs.service.commands;


import com.salesforce.tests.fs.service.Directory;
import com.salesforce.tests.fs.service.FileSystem;

public class TouchCommand implements Command {

    private FileSystem fileSystem;
    private Directory directory;


    public TouchCommand(String directory, FileSystem fileSystem) {
        this.fileSystem = fileSystem;
        this.directory = new Directory(directory, fileSystem.getCurrentDirectory(), true);
    }


    @Override
    public void execute() {
        fileSystem.createFile(directory);
    }
}
