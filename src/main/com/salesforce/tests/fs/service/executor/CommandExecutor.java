package com.salesforce.tests.fs.service.executor;

import com.salesforce.tests.fs.service.FileSystem;
import com.salesforce.tests.fs.service.commands.Command;
import com.salesforce.tests.fs.service.commands.CommandConstants;
import com.salesforce.tests.fs.service.commands.*;


public class CommandExecutor {

    private final int OPERATION = 0;
    private final int DIRECTORY = 1;


    public void executeCommand(String[] arguments, FileSystem fileSystem) {
        String operation = arguments[OPERATION];
        System.out.println("Command: "+operation);
        Command command;

        switch (operation) {
            case CommandConstants.LS:
                command = new LsCommand(fileSystem);
                break;
            case CommandConstants.MKDIR:
                command = new MkdirCommand(arguments[DIRECTORY], fileSystem);
                break;
            case CommandConstants.PWD:
                command = new PwdCommand(fileSystem);
                break;
            case CommandConstants.TOUCH:
                command = new TouchCommand(arguments[DIRECTORY], fileSystem);
                break;
            case CommandConstants.CD:
                command = new CdCommand(arguments[DIRECTORY], fileSystem);
                break;
            default:
                command  = new UnrecognizeCommand();
        }
        command.execute();
    }


}
